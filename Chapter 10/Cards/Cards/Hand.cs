using System;

namespace Cards
{
	class Hand
	{
        public const int HAND_SIZE = 13;
        private PlayingCard[] _cardSet = new PlayingCard[HAND_SIZE];
        private int _playingCardCount = 0;

		public void AddCardToHand(PlayingCard cardDealt)
		{
            // TODO: add the specified card to the hand
		}

		public override string ToString()
		{
			string result = "";
			foreach (PlayingCard card in this._cardSet)
			{
                result += $"{card.ToString()}\n";
            }

			return result;
		}
	}
}